import pytesseract
import os
from helpers import cv2_helpers as cv2_h
from PIL import Image
from multiprocessing import Pool


def find_text_in_image_async(text, image_folder):

    matched_data = matched_file = None
    images = os.listdir(image_folder)
    dataset = [(text, image_folder + image) for image in images]

    # Use if threading is done manually...
    # Agent pool will handle the work distribution

    # imgs_per_thread = total_images // threads
    # imgs_overhead = total_images % threads
    #
    # print(f'... Images {total_images} for total of {threads} threads')
    # print(f'... Sliding window of images per thread {imgs_per_thread}')
    # print(f'... Determining processing bounds')
    #
    # lower_bound = 0
    # upper_bound = lower_bound + imgs_per_thread - 1
    # process_bounds = [(lower_bound, upper_bound)]
    #
    # while upper_bound < total_images - imgs_per_thread:
    #
    #     lower_bound += imgs_per_thread
    #     upper_bound = lower_bound + imgs_per_thread - 1
    #     if upper_bound + imgs_per_thread > total_images:
    #         upper_bound += imgs_overhead
    #     process_bounds.append((lower_bound, upper_bound))
    #
    # for bound in process_bounds:
    #     print(bound)

    agents = 10
    with Pool(processes=agents) as pool:
        pool.map(lookup_text, dataset)

    matched_results = []
    for image in images:
        if image is not None:
            matched_results.append(image)
            print(f'Found match in file [{image}]')

    print('... Done')


def find_text_in_image_sync(text, image_folder):

    count = 0
    matched_data = matched_file = None
    images = os.listdir(image_folder)

    for image in images:

        image_path = image_folder + image

        imgs = cv2_h.normalize_image(image_path)
        image_text = ''

        for img in imgs:
            image_text += pytesseract.image_to_string(img).lower()
            image_text += ' '

        print(f'\n>>> leak [{count}] result \n {image_text}')

        count += 1
        if text in image_text:
            matched_file = image
            matched_data = image_text
            matched_imgs = imgs
            break

    print('\n\n-----------------------')
    print('        RESULTS        ')
    print('-----------------------')

    if matched_data and matched_file:
        print(f'Found a match in file {matched_file} with data: \n{matched_data}')

        print('... Opening image')
        matched_image_path = image_folder + matched_file
        target_img = Image.open(matched_image_path)
        target_img.show()

        for matched_img in matched_imgs:
            print(type(matched_img))
            pil_image = Image.fromarray(matched_img)
            pil_image.show()
    else:
        print('No image match found ...')


def lookup_text(*args):

    print(args)

    text, image = args[0]
    print(text, image)

    print(f'{text} in {image}')
    imgs = cv2_h.normalize_image(image)

    image_text = ''
    for img in imgs:
        image_text += pytesseract.image_to_string(img).lower()
        image_text += ' '

    if text in image_text:
        return image
    else:
        return None
