# player_count_ps4

Script finds player count for the game specified from the ps4 images leak and from internet.

Link with the zip file of images can be found in the following article:
 https://arstechnica.com/gaming/2018/12/sony-inadvertently-leaks-player-counts-for-ps4-titles/