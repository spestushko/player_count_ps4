import cv2
import numpy as np


def normalize_image(image_path):

    img = cv2.imread(image_path)

    img_bi = bin_image(img)
    img_bl = blur_image(img_bi)
    img_txt_slices = extract_text_regions(img_bl)

    resc_imgs = []
    for img_txt_slice in img_txt_slices:
        resc_imgs.append(rescale_image(img_txt_slice))

    resc_smth_imgs = []
    for resc_img in resc_imgs:
        resc_smth_imgs.append(blur_image(resc_img))

    # for resc_smth_img in resc_smth_imgs:
    #     cv2.imshow('smth', resc_smth_img)
    #
    # cv2.waitKey()
    # cv2.destroyAllWindows()

    return resc_smth_imgs


def rescale_image(img):
    rescaled = cv2.resize(img, None, fx=2, fy=2, interpolation=cv2.INTER_CUBIC)

    return rescaled


def bin_image(img):

    img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    _, mask = cv2.threshold(img_gray, 165, 255, cv2.THRESH_BINARY)
    image_final = cv2.bitwise_and(img_gray, img_gray, mask=mask)
    _, binary_image = cv2.threshold(image_final, 165, 255, cv2.THRESH_BINARY)

    # mean_c = cv2.adaptiveThreshold(img_gray, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 11, 4)
    # gaus_c = cv2.adaptiveThreshold(img_gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 121, 25)

    # cv2.imshow('bin_img', gaus_c)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    # return binary_image
    return binary_image


def blur_image(img):

    # blur = cv2.bilateralFilter(img, 3, 15, 15)
    blur = cv2.blur(img, (3, 3))
    # blur = cv2.GaussianBlur(img, (3, 3), 0)

    return blur


def extract_text_regions(img):

    kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (3, 3))
    dilated = cv2.dilate(img, kernel=kernel, iterations=9)
    image, contours, hierarchy = cv2.findContours(dilated, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

    text_slices = []

    print(len(contours))
    for contour in contours:

        [x, y, w, h] = cv2.boundingRect(contour)

        if (w < 35 and h < 35) or (w > 95 and h > 95):
            continue

        cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 255), 2)
        text_slice = img[y:y+h, x: x+w]

        text_slices.append(text_slice)

    return text_slices


def extract_text_region_v2(img):
    pass


'''
from helpers import cv2_helpers as cv2_h
cv2_h.normalize_image('myps4life/jtKtxPjFju.jpg')
'''
