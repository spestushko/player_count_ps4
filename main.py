from helpers import helpers
import text_lookup


def main():

    default_folder = 'myps4life/'

    helpers.print_banner()

    images_folder = input("Folder path to images [myps4life/]: ")
    if images_folder is '':
        images_folder = default_folder

    text_to_find = input("Find text: ").lower()
    text_lookup.find_text_in_image_async(text_to_find, images_folder, 10)


if __name__ == '__main__':
    main()
